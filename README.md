# README #

This nagios/check_mk plugin can be used to monitor a Fortinet FortiADC load-balancing appliance. It is currently only tested with the Layer-7 loadbalancer. (proxy mode). The current version is tested against FortiADC firmware 4.5.1, 4.5.2 and 4.6.0. It also returned perfdata on relevant functions. (not on license and uptime)

# License #

This software is released under a MIT-license. See LICENSE.txt for more details.

# Requirements #

## Python ##

This plugin is currently tested with python 2.6 and 2.7.

## Python Libraries ##

You will need to pip the 'requests' library. (http://docs.python-requests.org/en/latest/)

# Usage #

## Required Parameters ##

short | long | description
----- | ---- | ------------
 -H   | --hostname | hostname or ip adres for the device you want to monitor.
 -u   | --username | username for the user you added in the FortiADC to monitor ( recommend read-only account)
 -p   | --password | password for the monitoring user
 -C   | --check    | the check you want to execute. using -C list generates a list of available checks.

## Optional Parameters ##

short | long | description
----- | ---- | ------------
 -s   | --nossl | overrides the default, and uses http:// instead of https://
 -P   | --port | overrides the port for the connection, in case you changed the defaults.
 -c   | --critical | sets the value for when a check result is deemed critical, the default is 90.
 -w   | --warning  | sets the value for when a check result is deemed in warning, the default is 80.
 -F   | --filter   | filter currently used by healthcheck function. retrieves a specific backend or virtual server
 -m   | --minimumnodes | Minimum nodes for healthcheck, less then this amount skips the backend or virtual server. The default is 2.
 -v   | --version | prints version information, and exits
 -h   | --help | prints a short help screen, and exits.

## Available checks ##

check | description
----- | -----------
 cpu           | the -c and -w switches are treated as percentages, since the fortiADC reports in percentages
 memory        | the -c and -w switches are treated as percentages, since the fortiADC reports in percentages
 disk          | the -c and -w switches are treated as percentages, since the fortiADC reports in percentages
 uptime        | (the -c and -w switches are not used) It will go critical if the uptime is less then 1 hour, warning if less then 1 day.
 backendhealth | with the -F switch you can select a specific virtual server, with -m the minimum amount of nodes for it to be checked. the -c and -w switches are used as absolute values, so -w 4 and -c 2 will mean warnings with 4 healthy nodes, and critical with 2 healthy nodes
 license       | (the -c and -w switches are used as days). This checks the Firmware (FMWR)
 license_fmwr  | (the -c and -w switches are used as days). This checks the Firmware (FMWR).
 license_hdwr  | (the -c and -w switches are used as days). This checks the Hardware (HDWR).
 license_enhn  | (the -c and -w switches are used as days). This checks the Enhanced Support license (ENHN).
 license_comp  | (the -c and -w switches are used as days). This checks the Comprehensive Support (COMP).
 license_irdb  | (the -c and -w switches are used as days). This checks the IP Reputation (IRDB).
 license_addb  | (the -c and -w switches are used as days). This checks the ADDB (ADDB).
 license_ipge  | (the -c and -w switches are used as days). This checks the IPGE (IPGE).