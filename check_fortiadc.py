#!/usr/bin/env python
# check_fortiadc - written by Arjan Koole <arjan at kluner dot net>
# this software is provided 'as is'.
#
# tested with FortiADC 4.5.1, 4.5.2 and 4.6.0. but ONLY with L7 loadbalancing. (proxy)
# tested with python 2.6 and 2.7
#
# This software is opensource under an MIT style license. See LICENSE.txt for more details.
#
################################################################################
""" check_fortiadc is a nagios/check_mk monitoring plugin to monitor a Fortinet FortiADC loadbalancer """
""" it can be run from NRPE (Nagios) or MRPE on remote probes, or inside a nagios/check_mk instalation as active check """
# constants
__progname__    = 'check_fortiadc'
__version__     = '0.2.x-CURRENT'
__author__      = 'Arjan Koole <a.koole@uniserver.nl>'
__agent__       = "%s/%s" %(__progname__,__version__)
__headers__     = {'content-type': 'application/json', 'user-agent': (__agent__) }

# imports
import sys
import time
import datetime
import re
import urllib
import urllib3
import argparse
import json
import requests # bit of an issue on Mac OS X, very pip'able everywhere else, if it's not part of the distro/OS.
from requests.exceptions import ConnectionError

###### FUNCTIONS #####
def list_checks():
    ''' this function prints a list of checks this program offers '''
    print ""
    print "The following checks are currently available: "
    print " - cpu (the -c and -w switches are treated as percentages, since the fortiADC reports in percentages)"
    print " - memory (the -c and -w switches are treated as percentages, since the fortiADC reports in percentages)"
    print " - disk (the -c and -w switches are treated as percentages, since the fortiADC reports in percentages)"
    print " - uptime (the -c and -w switches are not used) It will go critical if the uptime is less then 1 hour, warning if less then 1 day."
    print " - backendhealth, with the -F switch you can select a specific virtual server, with -m the minimum amount of nodes for it to be checked"
    print "   the -c and -w switches are used as absolute values, so -w 4 and -c 2 will mean warnings with 4 healthy nodes, and critical with 2 healthy nodes"
    print " - license (the -c and -w switches are used as days). This checks the Firmware (FMWR)."
    print " - license_fmwr (the -c and -w switches are used as days). This checks the Firmware (FMWR)."
    print " - license_hdwr (the -c and -w switches are used as days). This checks the Hardware (HDWR)."
    print " - license_enhn (the -c and -w switches are used as days). This checks the Enhanced Support license (ENHN)."
    print " - license_comp (the -c and -w switches are used as days). This checks the Comprehensive Support (COMP)."
    print " - license_irdb (the -c and -w switches are used as days). This checks the IP Reputation (IRDB)."
    print " - license_addb (the -c and -w switches are used as days). This checks the ADDB (ADDB)."
    print " - license_ipge (the -c and -w switches are used as days). This checks the IPGE (IPGE)."

def connect():
    ''' this function creates a session with the fortiadc and retains the SessionID (sid) for usage in a check '''
    # see if we need to override the destination port or not.
    if args.port == None:
        hostname = args.hostname
    else:
        hostname = "%s:%s" %(args.hostname,args.port)
    # build the url
    url = "%s://%s/api/user/login" %(args.nossl,hostname)
    payload = {'username': (args.username), 'password': (args.password)}
    r = requests.post(url, json=payload, headers=__headers__, verify=args.nosslverify)
    global adccookie # store the cookie globally so it is accessible for other functions.
    adccookie = r.cookies
    pl = r.json()
    __headers__.update({ 'Authorization': 'Bearer ' + pl['token'] })
    httpcode = r.status_code
    if httpcode == 200:
        return(0)
    elif httpcode == 401:
        print "unauthorized: incorrect username or password."
        exit(1)
    else:
        print "Unknown failure."
        exit(1)

def check_cpu():
    ''' function to get the cpu consumption on the FortiADC '''
    # see if we need to override the destination port or not.
    if args.port == None:
        hostname = args.hostname
    else:
        hostname = "%s:%s" %(args.hostname,args.port)
    # connect to our API endpoint for this check
    url = "%s://%s/api/platform/resources_usage" %(args.nossl,hostname)
    r = requests.get(url, headers=__headers__, cookies=adccookie, verify=args.nosslverify)
    data = r.json()
    # cpu in data['payload']['cpu'], now compare with -c and -w values
    if data['payload']['cpu'] == "":
        print "CPU UNKNOWN"
        exit(3)
    elif data['payload']['cpu'] >= args.critical:
        # if the value equals or exceeds the critical value, we cast a critical output and return code. (2)
        print "CPU CRITICAL %s%% | CPU=%s%%;%s;%s;;" %(data['payload']['cpu'],data['payload']['cpu'],args.warning,args.critical)
        exit(2)
    elif data['payload']['cpu'] >= args.warning:
        # if the value equals or exceeds the warning value, we cast a critical output and return code. (1)
        print "CPU WARNING %s%% | CPU=%s%%;%s;%s;;" %(data['payload']['cpu'],data['payload']['cpu'],args.warning,args.critical)
        exit(1)
    else:
        # we're not exceeding thresholds, so we return OK with perfdata and exit 0.
        print "CPU OK %s%% | CPU=%s%%;%s;%s;;" %(data['payload']['cpu'],data['payload']['cpu'],args.warning,args.critical)
        exit(0)

def check_memory():
    ''' function to get the memory consumption on the FortiADC '''
    # see if we need to override the destination port or not.
    if args.port == None:
        hostname = args.hostname
    else:
        hostname = "%s:%s" %(args.hostname,args.port)
    # connect to our API endpoint for this check
    url = "%s://%s/api/platform/resources_usage" %(args.nossl,hostname)
    r = requests.get(url, headers=__headers__, cookies=adccookie, verify=args.nosslverify)
    data = r.json()
    # memory in data['payload']['ram'], now compare with -c and -w values
    if data['payload']['ram'] == "":
        print "MEMORY UNKNOWN"
        exit(3)
    elif data['payload']['ram'] >= args.critical:
        # if the value equals or exceeds the critical value, we cast a critical output and return code. (2)
        print "MEMORY CRITICAL %s%% | MEMORY=%s%%;%s;%s;;" %(data['payload']['ram'],data['payload']['ram'],args.warning,args.critical)
        exit(2)
    elif data['payload']['ram'] >= args.warning:
        # if the value equals or exceeds the warning value, we cast a critical output and return code. (1)
        print "MEMORY WARNING %s%% | MEMORY=%s%%;%s;%s;;" %(data['payload']['ram'],data['payload']['ram'],args.warning,args.critical)
        exit(1)
    else:
        # we're not exceeding thresholds, so we return OK with perfdata and exit 0.
        print "MEMORY OK %s%% | MEMORY=%s%%;%s;%s;;" %(data['payload']['ram'],data['payload']['ram'],args.warning,args.critical)
        exit(0)

def check_disk():
    ''' function to get the disk consumption on the FortiADC '''
    # see if we need to override the destination port or not.
    if args.port == None:
        hostname = args.hostname
    else:
        hostname = "%s:%s" %(args.hostname,args.port)
    # connect to our API endpoint for this check
    url = "%s://%s/api/platform/resources_usage" %(args.nossl,hostname)
    r = requests.get(url, headers=__headers__, cookies=adccookie, verify=args.nosslverify)
    data = r.json()
    # cpu in data['payload']['disk'], now compare with -c and -w values
    if data['payload']['disk'] == "":
        print "DISK UNKNOWN"
        exit(3)
    elif data['payload']['disk'] >= args.critical:
        # if the value equals or exceeds the critical value, we cast a critical output and return code. (2)
        print "DISK CRITICAL %s%% | DISK=%s%%;%s;%s;;" %(data['payload']['disk'],data['payload']['disk'],args.warning,args.critical)
        exit(2)
    elif data['payload']['disk'] >= args.warning:
        # if the value equals or exceeds the warning value, we cast a critical output and return code. (1)
        print "DISK WARNING %s%% | DISK=%s%%;%s;%s;;" %(data['payload']['disk'],data['payload']['disk'],args.warning,args.critical)
        exit(1)
    else:
        # we're not exceeding thresholds, so we return OK with perfdata and exit 0.
        print "DISK OK %s%% | DISK=%s%%;%s;%s;;" %(data['payload']['disk'],data['payload']['disk'],args.warning,args.critical)
        exit(0)

def check_uptime():
    ''' function to check the uptime on the FortiADC '''
    # Unfortunately, a FortiADC returns it's uptime in a bit of a nasty format. Maybe we should build a propper parser for it,
    # but for now that seems to be far to much effort, for far too little gain.
    # see if we need to override the destination port or not.
    if args.port == None:
        hostname = args.hostname
    else:
        hostname = "%s:%s" %(args.hostname,args.port)
    # build the url
    url = "%s://%s/api/platform/info" %(args.nossl,hostname)
    r = requests.get(url, headers=__headers__, cookies=adccookie, verify=args.nosslverify)
    data = r.json()
    uptimeplist = data['payload']['systemUpTime'].split(",")
    # first check
    if data['payload']['systemUpTime'] == "":
        print "UPTIME UNKNOWN, uptime string is emtpy!"
        exit(3)
    # the output from the REST API is unfortunately a bit brainless, I would much rather have had a number of seconds uptime, or epoch
    # since that is much easier to calculate with, not to mention: more accurate. Something for Fortinet to considerself.
    if uptimeplist[0] == "0d":
        # we have a zero days uptime, in the first hour. go critical, otherwise: warning. More then 1d: OK.
        if uptimeplist[1] == "0h":
            print "UPTIME CRITICAL, uptime under one day: %s" %(data['payload']['systemUpTime'])
            exit(2)
        else:
            print "UPTIME WARNING, uptime under one day: %s" %(data['payload']['systemUpTime'])
            exit(1)
    else:
        # we're not exceeding thresholds, so we return OK with perfdata and exit 0.
        print "UPTIME OK %s" %(data['payload']['systemUpTime'])
        exit(0)

def check_backendhealth():
    ''' function to get the health of the backends on the FortiADC '''
    # set some counters
    nodecount = 0
    healthynodes = 0
    unhealthynodes = 0
    badnodelist = ""
    # see if we need to override the destination port or not.
    if args.port == None:
        hostname = args.hostname
    else:
        hostname = "%s:%s" %(args.hostname,args.port)
    # connect to our API endpoint for this check
    url = "%s://%s/api/load_balance_pool" %(args.nossl,hostname)
    r = requests.get(url, headers=__headers__, cookies=adccookie, verify=args.nosslverify)
    data = r.json()
    # now we need to iterate through the data. We need to keep args.filter and args.minimumnodes in mind.
    # unfortunately we can not filter in the request itself for this API call.
    for virtuals in data['payload']:
        if args.filter is not None:
            ''' a filter for a backend was specified, so we only match and check that one '''
            if virtuals['mkey'] == args.filter:
                for members in virtuals['pool_member']:
                    if len(virtuals['pool_member']) > args.minimumnodes:
                        if members['hc_status'] != "1":
                            nodestatus = "X"
                            nodecount = (nodecount + 1)
                            unhealthynodes = (unhealthynodes + 1)
                            badnodelist += "%s: %s(%s), " %(virtuals['mkey'],members['server_name'],members['port'])
                        else:
                            nodestatus = "+"
                            nodecount = (nodecount + 1)
                            healthynodes = (healthynodes + 1)
                    else:
                        ''' this pool does not fill minimum node count criteria '''
                        pass
            else:
                ''' this mkey does not match the supplied filter, we ignore it '''
                pass
        else:
            ''' a filter for a backend was not specified, so we iterate through all '''
            for members in virtuals['pool_member']:
                if len(virtuals['pool_member']) > args.minimumnodes:
                    if members['hc_status'] != "1":
                            nodecount = (nodecount + 1)
                            unhealthynodes = (unhealthynodes + 1)
                            badnodelist += "%s: %s(%s), " %(virtuals['mkey'],members['server_name'],members['port'])
                    else:
                            nodecount = (nodecount + 1)
                            healthynodes = (healthynodes + 1)
                else:
                    ''' this pool does not fill minimum node count criteria '''
                    pass
    # now calculate the status to return and return it with aditional information
    if healthynodes <= args.critical:
        print "NODES CRITICAL %s of %s healthy | NODES=%s;%s;%s;0;%s" %(healthynodes,nodecount,healthynodes,args.warning,args.critical,nodecount)
        exit(2)
    elif healthynodes <= args.warning:
        print "NODES WARNING %s of %s healthy | NODES=%s;%s;%s;0;%s" %(healthynodes,nodecount,healthynodes,args.warning,args.critical,nodecount)
        exit(1)
    else:
        print "NODES OK %s of %s healthy | NODES=%s;%s;%s;0;%s" %(healthynodes,nodecount,healthynodes,args.warning,args.critical,nodecount)
        exit(0)

def check_license( lictype = "FMWR" ):
    ''' function to check the license status on the FortiADC '''
    # see if we need to override the destination port or not.
    if args.port == None:
        hostname = args.hostname
    else:
        hostname = "%s:%s" %(args.hostname,args.port)
    # connect to our API endpoint for this check
    url = "%s://%s/api/platform/license_info" %(args.nossl,hostname)
    r = requests.get(url, headers=__headers__, cookies=adccookie, verify=args.nosslverify)
    data = r.json()
    p = re.compile("Valid:")
    if p.match(data['payload']['licenseStatus']):
        ''' valid license, hurrah, pass along to further checks '''
        pass
    else:
        ''' invalid license, make noise and exit(2) '''
        print "LICENSE CRITICAL, license status: %s" %(data['payload']['licenseStatus'])
        exit(2)
    # next step, do some date magic to see how long before the licenses expire.
    # license types:
    #               HDWR : Hardware, can be emtpy and Unknown in the expire bit, if it's a VM.
    #               FMWR : Firmware
    #               ENHN : Enhanced Support
    #               COMP : Comprehensive support
    #               IRDB : IP Reputation
    #               ADDB : unknown
    #               IPGE : unknown
    #
    # first catch unknown licenses. This means either the license is unsupported (hardware license on a vm) or not purchased
    if ( data['payload']['fortiguard'][lictype]['expire'] == "Unknown"):
        print "LICENSE UNKNOWN for License %s" %(lictype)
        exit(3)
    expiredate = data['payload']['fortiguard'][lictype]['expire'].split("-")
    expireepoch = int(datetime.datetime(int(expiredate[0]),int(expiredate[1]),int(expiredate[2]),0,0).strftime('%s'))
    now = int(time.time())
    timeleft = int(expireepoch - now)
    timeleftindays = int(timeleft / 86400)
    # calculate if license has already expired
    if ( timeleft < 0 ):
        print "LICENSE CRITICAL, license %s has already expired (%s days ago)" %(lictype,timeleftindays)
        exit(2)
    # convert critical and warning days to seconds
    critical = ( args.critical * 86400 )
    warning = ( args.warning * 86400 )
    # calculate if one of the thresholds is exceeded
    if (timeleft < critical):
        print "LICENSE CRITICAL license %s expires in less then %s days on %s ( %s days)" %(lictype,args.critical,data['payload']['fortiguard'][lictype]['expire'],timeleftindays)
        exit(2)
    if (timeleft < warning):
        print "LICENSE WARNING license %s expires in less then %s days on %s (%s days)" %(lictype,args.warning,data['payload']['fortiguard'][lictype]['expire'],timeleftindays)
        exit(1)
    # still here? Awesome, our license is not going to expire too soon.
    print "LICENSE OK license %s expires on %s (%s days)" %(lictype,data['payload']['fortiguard'][lictype]['expire'],timeleftindays)
    exit(0)

def main():
    ''' main function '''
    global parser,args
    parser = argparse.ArgumentParser(description='%s %s written by %s' %(__progname__,__version__,__author__))
    parser.add_argument("-H","--hostname", required=True, help="hostname or ip address")
    parser.add_argument("-u","--username", required=True, help="username with access to the FortiADC REST API")
    parser.add_argument("-p","--password", required=True, help="password belonging to the suplied username")
    parser.add_argument("-C","--check", required=True, help="name of the check to perform (list to list available checks)")
    parser.add_argument("-s","--nossl", default="https", action="store_const", const="http", help="override the default for https://" )
    parser.add_argument("-k","--nosslverify", default=True, action="store_const", const=False, help="don't verify the ssl certificate" )
    parser.add_argument("-P","--port", type=int, help="override the default ports for http or https")
    parser.add_argument("-c", "--critical", type=int, default=90, help="value for the check to return critical (default: 90)")
    parser.add_argument("-w", "--warning", type=int, default=80, help="value for the check to return warning (default: 80)")
    parser.add_argument("-F", "--filter", help="filter for health check, to retrieve a specific backend or virtualserver.")
    parser.add_argument("-m", "--minimumnodes", type=int, default=1, help="minimum nodes for health check, less then this skips the backend or virtual server (default 2)")
    parser.add_argument("-v", "--version", action="version", version='%s version %s' %(__progname__,__version__), help="print version information and exit")
    args = parser.parse_args()

    # disable insecure request warning when nosslverify is set
    if not args.nosslverify:
       requests.packages.urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)

    # we can now check if the request check exists, and if so, creates
    # a session with the FortiADC, which can then be used to run a check
    try:
        connect()
    except ConnectionError as e:
        print "UNKNOWN: The initial connection to the FortADC Failed with: %s" %(e)
        exit(3)
    # connection has been established, now we try executing the specific call for our required information.
    if args.check == 'cpu':
        check_cpu()
    elif args.check == 'memory':
        check_memory()
    elif args.check == 'disk':
        check_disk()
    elif args.check == 'uptime':
        check_uptime()
    elif args.check == 'backendhealth':
        check_backendhealth()
    elif args.check == 'license':
        check_license()
    elif args.check == 'license_fmwr':
        check_license("FMWR")
    elif args.check == 'license_hdwr':
        check_license("HDWR")
    elif args.check == 'license_enhn':
        check_license("ENHN")
    elif args.check == 'license_comp':
        check_license("COMP")
    elif args.check == 'license_irdb':
        check_license("IRDB")
    elif args.check == 'license_addb':
        check_license("ADDB")
    elif args.check == 'license_ipge':
        check_license("IPGE")
    elif args.check == 'list':
        list_checks()
    else:
        print "unknown check command '%s' has been given, use list to see which are available." %(args.check)
        exit(3)

##########################################
# auto run the main function
if __name__ == "__main__":
    main() # will call the 'main' function only when you execute this from the command line.
